#!/usr/bin/python3
"""
Programa cliente UDP que abre un socket a un servidor
"""

import socket
import sys

try:
    server = sys.argv[1]
    c_port = int(sys.argv[2])
    reg = sys.argv[3]
    direccion = sys.argv[4]
    exprire_time = sys.argv[5]
    """ Asignacion de servidor, puerto, register, sip_address, expires_value"""

    mensaje1 = reg.upper() + " sip:" + direccion + " SIP/2.0\r\n\r\n"
    mensaje2 = "Expires:" + exprire_time + "\r\n\r\n"

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        """Crea el socket, lo configuramos y lo atamos a un servidor/puerto"""
        my_socket.connect((server, c_port))
        print("Enviando:", mensaje1 + "\n" + mensaje2)
        my_socket.send(mensaje1.encode('utf-8') + mensaje2.encode('utf-8'))
        # 1024 es el tamaño del buffer en bytes
        data = my_socket.recv(1024)
        print('Recibido -- ', data.decode('utf-8'))

    """
    Aquí se cerraria la conexion automaticamente
    """
    print("Socket terminado.")

except IndexError:
    print('Usage: client.py ip puerto register sip_address expires_value')
