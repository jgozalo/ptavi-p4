#!/usr/bin/python3
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import json
import socketserver
import sys
import time


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """ SIPRegisterHandler server class """

    diccionario_registro = {}
    """
    Inicializa un diccionario de diccionarios que guarda
    la informacion de los clientes
    (direccion, ip y hora de expiracion)
    """

    def register2json(self):
        """Genera un fichero JSON con los datos del diccionario"""
        with open('registered.json', 'w') as file:
            json.dump(self.diccionario_registro, file, indent=4)

    def json2register(self):
        """
        Comprueba si hay un fichero llamado 'registered.json'.
        Si existe, se usa como diccionario de usuarios registrados
        Si al leerlo obtiene cualquier error, no tendrá en cuenta
        dicho fichero
        """

        try:
            with open('registered.json', 'r') as file:
                self.diccionario_registro = json.load(file)
        except (FileNotFoundError):
            pass

    def handle(self):
        """
        Manejador de la clase SIPRegisterHandler
        (metodo que maneja todas las solicitudes)
        """
        self.json2register()
        mensaje_recibido = ''
        for line in self.rfile:
            mensaje_recibido += line.decode('utf-8')
            """
            Asigna la decodificacion del mensaje del cliente
            (convierte los Bytes a String)
            """

        # client_address es una tupla con los datos IP y puerto
        ip_cliente = self.client_address[0]
        puerto_cliente = self.client_address[1]
        """
        Client_address devuelve IP y puerto del cliente en forma de tupla
        """

        print("IP CLIENTE: " + str(ip_cliente) +
              "  /  PUERTO CLIENTE: " + str(puerto_cliente))
        print("Mensaje del cliente --> ", mensaje_recibido)

        info = mensaje_recibido.split(' ')
        """ Crea una lista de la division por espacios"""
        metodo = info[0]
        address = info[1]
        expire = info[2]
        """
        Asigna elementos de la lista a variables
        (metodo, direccion y caducidad)
        """

        if metodo == "REGISTER":
            """
            Comprueba que la peticion sea del tipo REGISTER
            """

            dic_cuenta = address.split(":")[1]
            """ Guarda la direccion despreciando 'sip:' """

            expire_int = int(expire.split(":")[1])
            """
            Guarda el tiempo de expiracion despreciando 'Expires'
            """

            tiempo_total = time.time() + expire_int
            """
            Suma del tiempo actual (en segundos desde 1970)
            con el tiempo de expiracion
            """

            hora_exp = time.strftime('%Y-%m-%d %H:%M:%S',
                                     time.gmtime(tiempo_total))
            hora_actual = time.strftime('%Y-%m-%d %H:%M:%S',
                                        time.gmtime(time.time()))
            """
            Devuelve hora de expiracion y actual en formato Y-m-d H:M:S
            """

            if expire_int > 0:
                self.diccionario_registro[dic_cuenta] = {"ip": ip_cliente,
                                                         "expires": hora_exp}
            elif expire_int == 0:
                try:
                    # borro cliente
                    del self.diccionario_registro[dic_cuenta]
                except KeyError:
                    pass
                print("El cliente " + dic_cuenta + " ha sido eliminado")

            """
            Da de alta o de baja en funcion del tiempo de expiracion.
            Añade si el tiempo de expiracion es mayor que 0 segundos
            Borra si es igual a 0, y el cliente existe en el diccionario
            Aunque el cliente no exista se simula su baja
            """

            for dic_cuenta in self.diccionario_registro.copy():
                if hora_actual >= \
                        self.diccionario_registro[dic_cuenta]['expires']:
                    del self.diccionario_registro[dic_cuenta]
            """Elimina los clientes que ''caducan''"""

            if self.diccionario_registro:
                print(self.diccionario_registro)
            """ No imprime {} si no hay clientes"""
            self.register2json()

            self.wfile.write(b'SIP/2.0 200 OK \r\n')
            """ Envia el mensaje de confirmacion al cliente"""


if __name__ == "__main__":

    try:
        sPORT = int(sys.argv[1])
        serv = socketserver.UDPServer(('', sPORT), SIPRegisterHandler)
        try:
            print("Lanzando servidor UDP de eco...")
            serv.serve_forever()
        except KeyboardInterrupt:
            print("Finalizado servidor")

    except IndexError:
        print('El formato debe ser: $python3 server.py puerto')
    except NameError and ValueError:
        print("No es posible asignar el puerto: " + sys.argv[1])

    """
    Lanza el servidor por tiempo indefinido.
    Solo se finalizara cuando se pulse Ctrl + C
    """
